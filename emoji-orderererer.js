const fs = require('fs');

const emojiJSON = require('./fixtures/emojis/index.json');

class EmojiOrderererer {
  static run() {
    fs.readFile('./emoji-ordering.txt', 'utf8', (err, data) => {
      if (err) throw err;
      const orderList = this.generateOrderList(data);
      const jsonString = this.generateOrderedJSON(orderList);
      fs.writeFile('./fixtures/emojis/index.json', jsonString);
    });
  }

  static generateOrderList(data) {
    return data.split("\n").splice(2).reduce((lines, line) => {
      if (!line) return lines;
      lines.push((line.match(/(?!U\+)[A-Z0-9]{4,5}/g) || []).join('-'));
      return lines;
    }, []);
  }

  static generateOrderedJSON(orderList) {
    return orderList.reduce((jsonString, unicode, index) => {
      const emojiKey = Object.keys(emojiJSON)
        .find(key => {return emojiJSON[key].unicode === unicode; });
      if (!emojiKey) return jsonString;
      return `${jsonString}"${emojiKey}":${JSON.stringify(emojiJSON[emojiKey])}${index === orderList.length - 1 ? '' : ','}`;
    }, '{') + '}';
  }
}

EmojiOrderererer.run();
